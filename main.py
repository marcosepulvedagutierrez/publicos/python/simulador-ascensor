import curses
import time
import threading
from enum import Enum
import keyboard
import curses

# Constantes
TOTAL_PISOS = 15
TIEMPO_ESPERA_EN_PISO = 5
TIEMPO_ESPERA_ENTRE_PISO = 9
TIEMPO_ESPERA_SIN_LLAMADO = 13

# Enum
class Direccion(Enum):
    SUBIENDO = "subiendo"
    BAJANDO = "bajando"


# Clase Ascensor
class Ascensor:

    def __init__(self, stdscr):
        self.terminado = False              # control de salida del programa
        self.piso_actual = 1                # inicia el ascensor en el piso 1
        self.detenciones_subida = set()     # lista con las peticiones de piso subida
        self.detenciones_bajada = set()     # Lista con las peticiones de piso bajada
        self.destinos = []                  # lista con las paradas segun sentido
        self.sentido = None                 # Sentido del viaje del ascensor
        self.tiempo_detenido = 0            # tiempo de detencion por piso
        self.tiempo_movimiento = 0          # tiempo de movimiento entre pisos
        self.tiempo_detenido_en_piso = 0    # tiempo de detencion en piso
        self.stdscr = stdscr              # pantalla de consola
        self.mensaje = "____linea de mensajes al usuario____"

    # Metodo publico que controla las peticiones
    def esperar_llamado(self):
        while not self.terminado:
            # if keyboard.is_pressed('esc'):  # Verificar si se ha presionado la tecla 'Esc'
            #     print("Salida del programa solicitada (presionando Esc).")
            #     self.terminado = True
            #     break
            
            pedido = input("")
            pedido = pedido.lower()
            # Verificamos ingreso de teclado pedido de ascensor
            # Q: permite salir de programa parando la ejecucion
            if pedido == 'q:':
                print("peticion de termino")
                self.terminado = True
                break

            # Verificacion de pidido de ascensor de S_ubida o B_ajada y piso de origen del llamado (ej: s12)
            elif pedido.startswith('s') or pedido.startswith('b'):
                try:
                    pedido_piso = int(pedido[1:])

                    # Para efectos del programa se verifica rango de pisos
                    if pedido_piso < 1 or pedido_piso > TOTAL_PISOS:
                        raise ValueError(f"El piso de la peticion especificado está fuera del rango. {pedido_piso}")
                    else:
                        self._agregar_una_detencion(pedido)

                except ValueError as e:
                    print(str(e))
            else:
                print("peticion de detencion no valida")


    def verificar_llamados(self):
        # print(f"ingrese verificar_llamados piso:{self.piso_actual} subida:{self.detenciones_subida} bajada:{self.detenciones_bajada}")
        titulo = f"Verificar llamados, piso_actual:{self.piso_actual} detenciones_subida:{self.detenciones_subida} detenciones_bajada:{self.detenciones_bajada}"
        self._dibujar_ascensor(titulo, ) 

        # # Solicitar al usuario que ingrese una cadena de texto
        # self.stdscr.addstr(0, 0, "Por favor, ingrese una cadena de texto:")
        # # Obtener la cadena de texto ingresada por el usuario
        # cadena = self.stdscr.getstr(1, 0)
        # # Imprimir la cadena de texto ingresada por el usuario
        # self.stdscr.addstr(1, 0, "Usted ingresó: " + cadena.decode('utf-8'))

        tecla = self.stdscr.getch()
        if tecla == curses.KEY_CANCEL:
            self.terminado = True

        # Verificamos que las listas de pedidos de ascensor esten vacias para verificar tiempo
        # de parada en el piso no exceda los 20s de lo contrario enviarlo al piso 1
        if not self.detenciones_bajada and not self.detenciones_subida:
            print("no hay peticiones")
            # self.mensaje = "no hay peticiones"
            # self._dibujar_ascensor()

            # Verificamos si esta parado en algun piso distinto al 1 para enviarlo devuelta
            if self.piso_actual != 1:
            
                print(f"ascensor parado en {self.piso_actual}. tiempo parado {self.tiempo_detenido}")
                # self.mensaje = f"ascensor parado en {self.piso_actual}. tiempo parado {self.tiempo_detenido}"
                # self._dibujar_ascensor()

                if self.tiempo_detenido >= TIEMPO_ESPERA_SIN_LLAMADO:
                    
                    print(f"Ascensor detenido por más de {TIEMPO_ESPERA_SIN_LLAMADO} segundos no hay llamados. Regresando al piso 1.")
                    # self.mensaje = "Ascensor detenido por más de 20 segundos no hay llamados. Regresando al piso 1."
                    # self._dibujar_ascensor()

                    self._agregar_una_detencion("b1")
                    self.sentido = Direccion.BAJANDO
                    self.tiempo_detenido = 0
                else:
                    self.tiempo_detenido += 1
            else:

                print(f"ascensor detenido 1")
                # self.mensaje = f"ascensor parado en {self.piso_actual}. tiempo parado {self.tiempo_detenido}"
                # self._dibujar_ascensor()

        else:
            # self.stdscr.addstr( "Verificando llamados")
            print(f"hay peticiones")

            if self.sentido == Direccion.SUBIENDO:
                print(f"subiendo, tiempo espera entre piso: {self.tiempo_movimiento} tiempo detenido en piso: {self.tiempo_detenido_en_piso}")
                if self.tiempo_movimiento == TIEMPO_ESPERA_ENTRE_PISO:
                    self.piso_actual += 1
                    self.tiempo_movimiento = 0
                elif self.tiempo_movimiento == 0:
                    print(f"Ascensor subiendo al piso: {list(self.detenciones_subida)[0]} piso actual: {self.piso_actual}")
                    if self.piso_actual == list(self.detenciones_subida)[0]:
                        print(f"Detener en piso {self.piso_actual} por {TIEMPO_ESPERA_EN_PISO}" )
                        if self.tiempo_detenido == TIEMPO_ESPERA_EN_PISO:
                            self.tiempo_movimiento = 0
                            self.tiempo_detenido = 0
                            self.detenciones_subida.remove(self.piso_actual)
                        else:
                            self.tiempo_detenido += 1
                    else:
                        self.tiempo_movimiento += 1
                else:
                    self.tiempo_movimiento += 1
                
            elif self.sentido == Direccion.BAJANDO:
                print(f"bajando tiempo espera entre piso: {self.tiempo_movimiento} tiempo detenido en piso: {self.tiempo_detenido_en_piso}")
                if self.tiempo_movimiento == TIEMPO_ESPERA_ENTRE_PISO:
                    self.piso_actual -= 1
                    self.tiempo_movimiento = 0

                elif self.tiempo_movimiento == 0:

                    print(f"Ascensor bajando al piso: {list(self.detenciones_bajada)[0]} piso actual: {self.piso_actual}")
                    if self.piso_actual == list(self.detenciones_bajada)[0]:

                        print(f"Detener en piso {self.piso_actual} por {TIEMPO_ESPERA_EN_PISO}" )
                        if self.tiempo_detenido_en_piso == TIEMPO_ESPERA_EN_PISO:
                            self.tiempo_movimiento = 0
                            self.tiempo_detenido_en_piso = 0
                            self.detenciones_bajada.remove(self.piso_actual)

                        else:
                            print(f"esperando en el piso: {self.piso_actual} tiempo: {self.tiempo_detenido_en_piso}")
                            self.tiempo_detenido_en_piso += 1

                    else:
                        print(f"No es piso {list(self.detenciones_bajada)[0]} es piso: {self.piso_actual}, debe seguir bajando")
                        self.tiempo_movimiento += 1

                else:
                    self.tiempo_movimiento += 1
            
            else:
                if self.detenciones_subida:
                    self.sentido = Direccion.SUBIENDO
                    print(f"lo enviamos a subir")

                elif self.detenciones_bajada:
                    self.sentido = Direccion.BAJANDO
                    print(f"lo enviamos a bajar")
                else:
                    print(f"No hay llamados")



    # Metodo privado que agrega las peticiones por piso
    def _agregar_una_detencion(self, piso_pedido):

        # evaluacion de detenciones por pedido de ascensor y su clasificacion por sentido
        if piso_pedido.startswith('s'):
            self.detenciones_subida.add(int(piso_pedido[1:]))
        else:
            self.detenciones_bajada.add(int(piso_pedido[1:]))


    # def agregar_un_llamado(self, piso_destino):
    #     print(f"Piso Actual: {self.piso_actual}")
    #     if piso_destino > self.piso_actual:
    #         self.direccion = Direccion.SUBIENDO
    #     elif piso_destino < self.piso_actual:
    #         self.direccion = Direccion.BAJANDO
    #     else:
    #         print(f"Ascensor detenido en el piso {self.piso_actual}")
    #         return

    #     print(f"Llamado al piso {piso_destino} en dirección {self.direccion}")
    #     self.llamados.append((piso_destino, self.direccion))
    #     self.llamados = sorted(set(self.llamados), key=self.llamados.index)

    #     while self.llamados:
    #         siguiente_piso, direccion = self.llamados.pop(0)
    #         print(
    #             f"Ascensor moviéndose {direccion} hacia el piso {siguiente_piso}")
    #         self._mover_a_piso(siguiente_piso, direccion)


    # def _mover_a_piso(self, piso_destino, direccion_destino ):
    #     if direccion_destino == Direccion.SUBIENDO:
    #         for piso in range(self.piso_actual + 1, piso_destino + 1):
    #             print(f"Ascensor subiendo a piso {piso}")
    #             self.piso_actual = piso
    #             time.sleep(TIEMPO_ESPERA_EN_PISO)
    #     elif direccion_destino == "abajo":
    #         for piso in range(self.piso_actual - 1, piso_destino - 1, -1):
    #             print(f"Ascensor bajando al piso {piso}")
    #             self.piso_actual = piso
    #             time.sleep(TIEMPO_ESPERA_EN_PISO)

    #     print(f"Ascensor llegó al piso {self.piso_actual}")

    #     self.tiempo_detenido = 0


    def _dibujar_ascensor(self, titulo = "", mensaje = ""):
        separacion_y = 0
        separacion_x = 1

        # Limpiar la pantalla
        self.stdscr.clear()

        # Dibuja el titulo en la linea cero de la pantalla
        self.stdscr.addstr(separacion_y, 0, titulo)
        # Dibuja el mensaje en la linea uno de la pantalla
        separacion_y +=1
        self.stdscr.addstr(separacion_y, 0, mensaje)
        separacion_y += 1
        self.stdscr.addstr(separacion_y, 0, "Piso    : " + str(self.piso_actual))
        separacion_y += 1
        self.stdscr.addstr(separacion_y, 0, "D. Sub  : " + str(self.detenciones_subida))
        separacion_y += 1
        self.stdscr.addstr(separacion_y, 0, "D. Baj  : " + str(self.detenciones_bajada))
        separacion_y += 1
        if self.sentido is not None:
            self.stdscr.addstr(separacion_y, 0, "Sentido : " + str(self.sentido.value))
        else:
            self.stdscr.addstr(separacion_y, 0, "Sentido : No definido")
        separacion_y += 1
        self.stdscr.addstr(separacion_y, 0, "Timer   : " + str(self.tiempo_movimiento))
        separacion_y += 1
        self.stdscr.addstr(separacion_y, 0, "Termino : " + str(self.terminado))
        separacion_y += 2

        # Dibujar todos los pisos del ascensor con el ascensor en el piso actual
        for rango in range(1, TOTAL_PISOS+1):
        
            if rango == self.piso_actual:
                self.stdscr.addstr((TOTAL_PISOS - rango)+separacion_y, separacion_x, f"[X] {rango}")
            else:
                self.stdscr.addstr((TOTAL_PISOS - rango)+separacion_y, separacion_x, f"[ ] {rango}" )

        # Actualizar la pantalla
        self.stdscr.refresh()


def main(stdscr):
    print("main")
    # ascensor = Ascensor()
    ascensor = Ascensor(stdscr)

    ascensor._agregar_una_detencion("s5")
    ascensor._agregar_una_detencion("s7")
    ascensor._agregar_una_detencion("b3")
    # thread = threading.Thread(target=ascensor.esperar_llamado)
    # thread.start()

    stdscr.clear()  # Limpiar la pantalla
    while not ascensor.terminado:
        ascensor.verificar_llamados()
        time.sleep(1)
        # Dibujar el ascensor en modo gráfico
        # ascensor._dibujar_ascensor("inicio")

    stdscr.clear()  # Limpiar la pantalla
    print("Fin del programa")
    # thread.join()

if __name__ == "__main__":
    curses.wrapper(main)
    # main()